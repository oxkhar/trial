<?php

namespace Build;

$loader = require __DIR__ . '/../vendor/autoload.php';

class Install
{
    const SCHEMA_FILE   = __DIR__ . "/../app/db/users-structure.sql";

    const FIXTURES_FILE = __DIR__ . "/../app/db/users-fixtures.sql";

    /**
     * @val \PDO
     */
    protected $dbm;

    public function __construct(\SB\MA\Trial\Core\Container $container)
    {
        $this->container = $container;
    }

    public static function run($di)
    {
        $cmd = new self($di);

        $cmd->makeFiles();
        $cmd->createDataStructures();
        $cmd->loadDataFixtures();
    }

    public function makeFiles()
    {
        echo "Make build paths..." . PHP_EOL;

        array_map(
            function ($pathDir) {
                is_dir($pathDir) || mkdir($pathDir, 0777, true);
            },
            [
                __DIR__ . "/../build/data",
                __DIR__ . "/../build/tests",
                __DIR__ . "/../build/coverage"
            ]
        );
    }

    public function createDataStructures()
    {
        echo "Create data structures..." . PHP_EOL;

        $this->container->userDbm->exec(file_get_contents(static::SCHEMA_FILE));
    }

    public function loadDataFixtures()
    {
        echo "Loading data fixtures for users" . PHP_EOL;

        $this->container->userDbm->exec(file_get_contents(static::FIXTURES_FILE));
    }
}

Install::run(require __DIR__ . '/../app/dependencies.php');
