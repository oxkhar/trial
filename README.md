[![StyleCI](https://styleci.io/repos/58338095/shield?branch=master)](https://styleci.io/repos/58338095)

# Aplicación Web - API Rest

Aplicación web de identificación de usuarios y control de acceso a los contenidos.

API Rest de gestión de los usuarios acceso a la aplicación

## Requerimientos

* **PHP 5.6** en su versión de linea de comandos (php5-cli)
* Los siguieentes módulos.
  * sqlite
  * json
  * curl (para los tests funcionales)
* [Composer](https://getcomposer.org/)

## Instalación

### Automática

```
./build.sh
```

### Manual

* **PHP**: Para sistemas basados en Debian (Ubuntu,...)

```
apt-get install php5-cli php5-sqlite php5-curl php5-json
```

* **Composer**

```
php -r 'readfile("https://getcomposer.org/installer");' |  php -- --install-dir=bin --version=1.1.2 --filename=composer
```

## Despliegue

```
bin/composer install
```

### Usuarios generados...

|   name    | password | roles  |
|-----------|----------|--------|
| supercoco | 123456   |   -    |
| caponata  | asdfg    | PAGE_1 |
| gustavo   | zxcvb    | PAGE_2 |
| epi       | 98765    |PAGE_1|PAGE_3|
| blas      | sesamo   |PAGE_1,PAGE2,PAGE_3|
| triki     | qwerty   | ADMIN  |


## Ejecución

```
php -S localhost:8080 -t www www/index.php
```

### Aplicación Web
Accede con un navegador web a ...
* http://localhost:8080/login
* http://localhost:8080/page1
* http://localhost:8080/page2
* http://localhost:8080/page3

#### API Rest

* http://localhost:8080/api/users  ```[GET]```
* http://localhost:8080/api/users/{uid}  ```[GET]```
* http://localhost:8080/api/users  ```[POST]```
```
Content:
{
  "name": "{name}",
  "password": "{password}",
  "roles": ["{role1}","{role2}",...],
}
```

* http://localhost:8080/api/users/{uid} [PUT]

```
Content:
{
  "uid": "{uid}",
  "name": "{name}",
  "password": "{password}",
  "roles": ["{role1}","{role2}",...],
}
```

* http://localhost:8080/api/users/{uid} [DELETE]

## Tests

* Ejecución de los tests **unitarios**...

```
bin/phpunit
```

> Informe de cobertura de tests en ```build/coverage/index.html```

* Ejecución de tests **funcionales**...

```
bin/phpunit -c phpunit-functional.xml
```
