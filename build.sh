#!/bin/bash

PATH_PWD=$(pwd)
SCRIPT_PATH=$(cd $(dirname $0); pwd)
SCRIPT_NAME=$(basename $0)
PHP_BIN=($(which php5 php 2> /dev/null))

echo -e "\n### Iniciando proceso de instalación...\n\n"

CMD_COMPOSER=($(which ${SCRIPT_PATH}/bin/composer))
if [[ ${CMD_COMPOSER} == "" ]]; then
    ${PHP_BIN} -r 'readfile("https://getcomposer.org/installer");' |  ${PHP_BIN} -- --install-dir="${SCRIPT_PATH}/bin" --version=1.1.2 --filename=composer
    CMD_COMPOSER="${SCRIPT_PATH}/bin/composer"
fi

${PHP_BIN} $CMD_COMPOSER install
EXIT_CODE=$?
if [[ $EXIT_CODE != 0 ]]; then
    echo -e "\n Revisa la configuración de tu sistema para la correcta instalación de la aplicación\n"
    exit $EXIT_CODE
fi

#
echo -e "\n\n### Pasando tests y generando informes...\n"

CMD_PHPUNIT=${SCRIPT_PATH}/bin/phpunit

echo -e "  Pasando tests unitarios...\n"
${PHP_BIN} $CMD_PHPUNIT -c ${SCRIPT_PATH}

echo -e "\n * (See build/coverage/index.html) * "

echo -e "\n\n  Pasando tests funcionales...\n"
${PHP_BIN} $CMD_PHPUNIT -c ${SCRIPT_PATH}/phpunit-functional.xml

