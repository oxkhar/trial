<?php

namespace SB\MA\Trial\Exceptions;

class UserNotFoundException extends ApplicationException
{
    public function __construct($message = "Not Found")
    {
        parent::__construct($message, 404);
    }
}
