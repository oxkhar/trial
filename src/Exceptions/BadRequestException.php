<?php

namespace SB\MA\Trial\Exceptions;

class BadRequestException extends ApplicationException
{
    public function __construct($message = "Bad Request")
    {
        parent::__construct($message, 400);
    }
}
