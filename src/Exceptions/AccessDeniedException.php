<?php

namespace SB\MA\Trial\Exceptions;

class AccessDeniedException extends ApplicationException
{
    public function __construct($message = "Forbidden")
    {
        parent::__construct($message, 403);
    }
}
