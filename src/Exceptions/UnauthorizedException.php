<?php

namespace SB\MA\Trial\Exceptions;

class UnauthorizedException extends ApplicationException
{
    public function __construct($message = "Unauthorized")
    {
        parent::__construct($message, 401);
    }
}
