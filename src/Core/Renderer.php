<?php

namespace SB\MA\Trial\Core;

/**
 * Render content of templates with tag values
 */
class Renderer
{
    /**
     * @var string
     */
    private $pathTemplates;

    public function __construct(array $config)
    {
        $this->pathTemplates = rtrim($config['path'], '/');
    }

    /**
     * Generate output of template with param values
     *
     * @param string $template
     * @param array $params
     * @return string
     */
    public function render($template, array $params = [])
    {
        $file = $this->pathTemplates . '/' . $template . '.php';

        if (!file_exists($file)) {
            return false;
        }

        return $this->buffer($file, $params);
    }

    /**
     * Method without params for no mixing with params values
     *
     * @param string $file
     * @param array $params
     * @return string
     */
    protected function buffer()
    {
        extract(func_get_arg(1));

        ob_start();
        include func_get_arg(0);

        return ob_get_clean();
    }
}
