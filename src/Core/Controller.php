<?php

namespace SB\MA\Trial\Core;

abstract class Controller
{
    /**
     * @var Container
     */
    protected $container;
    /**
     * @var string
     */
    protected $payload;
    /**
     * @var \ArrayObject
     */
    private $request;

    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->request = new \ArrayObject($_REQUEST, \ArrayObject::ARRAY_AS_PROPS);
    }

    public static function redirect($location, $permanent = false)
    {
        header('Location: ' . $location, true, $permanent ? 301 : 302);
        exit();
    }

    abstract public function error($message, $status);

    abstract public function render(...$render);

    /**
     * Content body of a request
     * @return string
     */
    public function payload()
    {
        if (!isset($this->payload)) {
            $this->payload = file_get_contents("php://input");
        }

        return $this->payload;
    }

    /**
     * @return array
     */
    public function request()
    {
        return $this->request->getArrayCopy();
    }

    /**
     * @param string $attribute
     * @return string
     */
    protected function requestValue($attribute)
    {
        return isset($this->request[$attribute]) ? $this->request[$attribute] : null;
    }

    /**
     * @return string
     */
    protected function requestMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}
