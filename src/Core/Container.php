<?php

namespace SB\MA\Trial\Core;

/**
 * Simple container to define services that will be instanced
 * when it's invoked
 */
class Container extends \ArrayObject
{
    /**
     *
     * @var \ArrayObject
     */
    private $container;

    public function __construct()
    {
        parent::__construct([], \ArrayObject::ARRAY_AS_PROPS);

        $this->container = new \ArrayObject();
    }

    /**
     * To set services like array is assigned...
     * <code>$container['service] = [...]</code>
     *
     * @param string $service Name identified a service
     * @param Closure $factory A callable factory to instance the service
     * @throws \RuntimeException when factory is not a value callable like a function
     */
    public function offsetSet($service, $factory)
    {
        if (!is_callable($factory)) {
            throw new \RuntimeException("Service $service has not defined a properly factory");
        }

        $this->container[$service] = $factory->bindTo($this);
    }

    /**
     * Can access services through container like array...
     * <code>$service = $container['service'];</code>
     *
     * @param string $service
     * @return mixed Service instance
     * @throws \RuntimeException when a service is unavailable
     */
    public function offsetGet($service)
    {
        if (!isset($this->container[$service])) {
            throw new \RuntimeException("Service $service not defined");
        }
        if (!isset($this[$service])) {
            parent::offsetSet($service, $this->container[$service]());
        }

        return parent::offsetGet($service);
    }
}
