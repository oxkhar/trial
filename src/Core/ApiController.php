<?php

namespace SB\MA\Trial\Core;

use SB\MA\Trial\Exceptions\UnauthorizedException;
use SB\MA\Trial\Manager\ControlAccessManager;
use SB\MA\Trial\Model\Resource;

class ApiController extends Controller
{
    public function error($message, $code)
    {
        header('Date: ' . (new \DateTime())->format(\DateTime::RFC822));

        return $this->render(['message' => $message], $code);
    }

    /**
     * Render a array structure into a json string representation
     * @param array $data
     * @param int $code
     * @return string
     */
    public function render(...$render)
    {
        list($data, $code) = array_replace([[], 200], $render);

        isset($code) && http_response_code($code);

        $response = "";
        if (!empty($data)) {
            header("Content-type: application/json");
            $response = json_encode($data, JSON_PRETTY_PRINT);
        }

        return $response;
    }

    /**
     * Verifies authentication for a request to a resource
     * @param string $resource
     * @throws UnauthorizedException
     */
    protected function authentication($resource)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header('WWW-Authenticate: Basic realm="Users API Rest"');
            throw new UnauthorizedException();
        }

        /* @var $controlAccess ControlAccessManager */
        $controlAccess = $this->container->controlAccess;

        $user = $controlAccess->authenticateMe($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);

        $method = $this->requestMethod();

        $location = new Resource($resource);
        $controlAccess->authorizeMe($location, $user, $method);
    }
}
