<?php

namespace SB\MA\Trial\Core;

use SB\MA\Trial\Manager\SessionManager;
use SB\MA\Trial\Model\Resource;
use SB\MA\Trial\Model\User;

class WebController extends Controller
{
    /**
     * @var SessionManager
     */
    protected $session;

    public function __construct(Container $container)
    {
        parent::__construct($container);

        $this->session = $container->session;
        if ($_SERVER['PATH_INFO'] != "/login") {
            $this->session->referer = $_SERVER['PATH_INFO'];
        }
    }

    /**
     * @param string $message
     * @param int $code
     * @return string
     */
    public function error($message, $code)
    {
        return $this->render('error', ['message' => $message], $code);
    }

    /**
     * @param string $template
     * @param array $params
     * @param int $code
     * @return string
     */
    public function render(...$render)
    {
        list($template, $params, $code) = array_replace(['default', [], 200], $render);

        isset($code) && http_response_code($code);

        return $this->container->render->render($template, $params);
    }

    /**
     * @return User
     */
    protected function userLogged()
    {
        return $this->session->userLogged();
    }

    /**
     * @return boolean
     */
    protected function isLogged()
    {
        return $this->session->isLogged();
    }

    /**
     * @return string
     */
    protected function referrer()
    {
        return isset($this->session->referer) ? $this->session->referer : '';
    }

    protected function checkAccess(User $user, Resource $location)
    {
        $this->container->controlAccess->authorizeMe($location, $user);
    }
}
