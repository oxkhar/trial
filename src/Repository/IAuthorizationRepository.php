<?php

namespace SB\MA\Trial\Repository;

use SB\MA\Trial\Model\Authorization;

interface IAuthorizationRepository
{
    /**
     * @param string $resourceName
     * @return Authorization
     */
    public function findAuthorizationByResourceName($resourceName);
}
