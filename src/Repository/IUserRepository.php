<?php

namespace SB\MA\Trial\Repository;

use SB\MA\Trial\Model\User;
use SB\MA\Trial\Model\UUID;

interface IUserRepository
{
    public function findUserByNameAndPassword($name, $password);

    public function findUserByUid(UUID $uid);

    public function findAllUsers();

    public function create(User $user);

    public function update(User $user);

    public function delete(UUID $uid);
}
