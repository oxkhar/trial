<?php

namespace SB\MA\Trial\Repository;

use SB\MA\Trial\Model\User;
use SB\MA\Trial\Model\UUID;

class UserRepository implements IUserRepository
{
    private $dbm;

    public function __construct(\PDO $dbm)
    {
        $this->dbm = $dbm;
    }

    public function nextId()
    {
        return UUID::generate();
    }

    public function findUserByNameAndPassword($name, $password)
    {
        $stm = $this->dbm->prepare(
            "SELECT * FROM users " .
            "WHERE name = :name and password = :password"
        );

        $stm->execute([
            ':name'     => $name,
            ':password' => $password
        ]);
        $row = $stm->fetch();

        $stm->closeCursor();

        if (empty($row)) {
            return null;
        }

        return $this->newUser($row);
    }

    /**
     * @param array|\ArrayAccess $row
     * @return User
     */
    protected function newUser($row)
    {
        $row['uid']   = new UUID($row['uid']);
        $row['roles'] = \json_decode($row['roles']);

        return User::bind($row);
    }

    /**
     * @param UUID $uid
     * @return User
     */
    public function findUserByUid(UUID $uid)
    {
        $stm = $this->dbm->prepare(
            "SELECT * FROM users " .
            "WHERE uid = :uid"
        );

        $stm->execute([
            ':uid' => (string)$uid
        ]);
        $row = $stm->fetch();

        $stm->closeCursor();

        if (empty($row)) {
            return null;
        }

        return $this->newUser($row);
    }

    public function findAllUsers()
    {
        $stm = $this->dbm->query("SELECT * FROM users");
        foreach ($stm as $row) {
            yield $this->newUser($row);
        }
        $stm->closeCursor();
    }

    public function create(User $user)
    {
        $stm = $this->dbm->prepare(
            'INSERT INTO users (uid, name, password, roles) ' .
            'VALUES (:uid, :name, :password, :roles)'
        );

        $stm->execute($this->newParams($user));
    }

    protected function newParams(User $user)
    {
        return array_replace(
            $this->updateParams($user),
            [
                ':password' => $user->password(),
            ]
        );
    }

    protected function updateParams(User $user)
    {
        return [
            ':uid'   => $user->uid(),
            ':name'  => $user->name(),
            ':roles' => json_encode($user->roles())
        ];
    }

    public function update(User $user)
    {
        $stm = $this->dbm->prepare(
            'UPDATE users SET ' .
            ' name = :name, ' .
            ' roles = :roles ' .
            'WHERE uid = :uid'
        );

        $stm->execute($this->updateParams($user));
    }

    public function delete(UUID $uid)
    {
        $stm = $this->dbm->prepare(
            'DELETE FROM users ' .
            'WHERE uid = :uid'
        );

        $stm->execute([':uid' => $uid]);
    }
}
