<?php

namespace SB\MA\Trial\Repository;

use SB\MA\Trial\Model\Authorization;
use SB\MA\Trial\Model\Permission;
use SB\MA\Trial\Model\Role;

class AuthorizationRepository implements IAuthorizationRepository
{
    /**
     * @var array
     */
    private $authorizations;

    public function __construct(array $authorizations)
    {
        $this->authorizations = [];

        $fnc = function ($permission) {
            $permission = (array)$permission;

            return [
                $permission[0],
                (array)(isset($permission[1]) ? $permission[1] : "*")
            ];
        };

        foreach ($authorizations as $name => $permissions) {
            $this->authorizations[$name] = array_map(
                $fnc,
                (array)$permissions
            );
        }
    }

    public function findAuthorizationByResourceName($resourceName)
    {
        if (!isset($this->authorizations[$resourceName])) {
            return null;
        }

        if ($this->authorizationIsNotInstantiated($resourceName)) {
            $this->authorizations[$resourceName] = $this->newAuthorization($resourceName);
        }

        return $this->authorizations[$resourceName];
    }

    private function authorizationIsNotInstantiated($resourceName)
    {
        return is_array($this->authorizations[$resourceName]);
    }

    private function newAuthorization($resourceName)
    {
        return new Authorization(
            $resourceName,
            array_map(
                function ($permission) {
                    return new Permission(new Role($permission[0]), $permission[1]);
                },
                $this->authorizations[$resourceName]
            )
        );
    }
}
