<?php

namespace SB\MA\Trial\Model;

class User implements \JsonSerializable
{
    /**
     * @var UUID
     */
    private $uid;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $password;
    /**
     * @var Roles[]
     */
    private $roles;

    public function __construct(UUID $uid, $name, $roles)
    {
        $this->uid   = $uid;
        $this->name  = $name;
        $this->roles = (array)$roles;
    }

    /**
     * @param array|ArrayAccess $data
     * @return User
     */
    public static function bind($data)
    {
        $model = new self($data['uid'], $data['name'], $data['roles']);

        isset($data['password']) && $model->changePassword($data['password']);

        return $model;
    }

    public function changePassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return UUID
     */
    public function uid()
    {
        return $this->uid;
    }

    public function name()
    {
        return $this->name;
    }

    public function password()
    {
        return $this->password;
    }

    /**
     * @return array
     */
    public function roles()
    {
        return $this->roles;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'uid'   => (string)$this->uid,
            'name'  => $this->name,
            'roles' => $this->roles
        ];
    }
}
