<?php

namespace SB\MA\Trial\Model;

class Role
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function name()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name;
    }
}
