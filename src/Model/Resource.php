<?php

namespace SB\MA\Trial\Model;

class Resource
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function name()
    {
        return $this->name;
    }
}
