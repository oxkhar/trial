<?php

namespace SB\MA\Trial\Model;

class Permission
{
    private $role;
    private $actions;

    public function __construct(Role $role, $actions = "GET")
    {
        $this->role    = $role;
        $this->actions = (array)$actions;
    }

    public static function universal()
    {
        return new self(new Role('*'), '*');
    }

    public function role()
    {
        return $this->role;
    }

    public function actions()
    {
        return $this->actions;
    }
}
