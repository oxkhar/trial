<?php

namespace SB\MA\Trial\Model;

class Authorization
{
    /**
     * @var string
     */
    private $location;
    /**
     * @var Permission[]
     */
    private $permissions;

    public function __construct($location, array $permissions)
    {
        $this->location    = $location;
        $this->permissions = $permissions;
    }

    /**
     * @return string
     */
    public function location()
    {
        return $this->location;
    }

    /**
     * @return Permission[]
     */
    public function permissions()
    {
        return $this->permissions;
    }
}
