<?php

namespace SB\MA\Trial\Controller;

use SB\MA\Trial\Core\WebController;
use SB\MA\Trial\Model\Resource;

class PageController extends WebController
{
    public function actionIndex($name)
    {
        if (!$this->isLogged()) {
            $this->redirect('login');
        }

        $user = $this->userLogged();
        $page = new Resource($name);

        $this->checkAccess($user, $page);

        return $this->render(
            'page',
            [
                'page' => $page,
                'user' => $user,
            ]
        );
    }
}
