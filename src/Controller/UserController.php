<?php

namespace SB\MA\Trial\Controller;

use SB\MA\Trial\Core\ApiController;
use SB\MA\Trial\Exceptions\BadRequestException;
use SB\MA\Trial\Manager\UserManager;

class UserController extends ApiController
{
    const URI_BASE = '/api/users';

    /**
     * @var UserManager
     */
    private $manager;

    public function actionList()
    {
        $this->init();

        return $this->render($this->manager->listUsers());
    }

    private function init()
    {
        $this->authentication('users');

        $this->manager = $this->container->userManager;
    }

    public function actionGet($uid)
    {
        $this->init();

        $user = $this->manager->giveMeAUser($uid);

        return $this->render($user);
    }

    public function actionPost()
    {
        $this->init();

        $userData = json_decode($this->payload(), true);
        if (empty($userData)) {
            $message = json_last_error() ? json_last_error_msg() : "User data not valid";
            throw new BadRequestException($message);
        }

        $user = $this->manager->addNewUser($userData);

        header('Location: ' . static::URI_BASE . '/' . $user->uid());
        header('Content-Location: ' . static::URI_BASE . '/' . $user->uid());

        return $this->render($user, 201);
    }

    public function actionDelete($uid)
    {
        $this->init();

        $this->manager->remove($uid);

        return $this->render(null, 204);
    }

    public function actionPut($uid)
    {
        $this->init();

        $userData = json_decode($this->payload(), true);

        $this->manager->updateContent($uid, $userData);

        return $this->render(null, 204);
    }
}
