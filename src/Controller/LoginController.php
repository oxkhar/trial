<?php

namespace SB\MA\Trial\Controller;

use SB\MA\Trial\Core\WebController;
use SB\MA\Trial\Exceptions\UserNotFoundException;

class LoginController extends WebController
{
    public function actionLogin()
    {
        $data = $this->login();

        return $this->render('login', $data);
    }

    private function login()
    {
        $data = [
            'name'    => $this->requestValue('name'),
            'message' => ''
        ];

        if ($this->requestMethod() != 'POST') {
            return $data;
        }

        try {
            $this->session->login($this->requestValue('name'), $this->requestValue('password'));
            $this->redirectReferrerIfExists();

            $data['message'] = "OK: User '" . $this->requestValue('name') . "' is valid";
        } catch (UserNotFoundException $exc) {
            $data['message'] = "ERROR: User not valid!";
        }

        return $data;
    }

    private function redirectReferrerIfExists()
    {
        if (!empty($this->referrer())) {
            $this->redirect($this->referrer());
        }
    }

    public function actionLogout()
    {
        $this->session->logout();

        $this->redirect('/login', true);
    }
}
