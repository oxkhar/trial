<?php

namespace SB\MA\Trial\Manager;

use SB\MA\Trial\Exceptions\UserNotFoundException;
use SB\MA\Trial\Model\User;
use SB\MA\Trial\Repository\IUserRepository;

/**
 * Handle user session
 */
class SessionManager extends \ArrayObject
{
    /**
     * @var IUserRepository
     */
    private $userRepository;
    private $lifetime;

    /**
     * @param IUserRepository $userRepository
     * @param array $config
     */
    public function __construct(IUserRepository $userRepository, array $config = null)
    {
        parent::__construct([], \ArrayObject::ARRAY_AS_PROPS);

        $this->setConfig((array)$config);
        $this->userRepository = $userRepository;

        register_shutdown_function([$this, 'close']);

        $this->sessionStart();
        if ($this->userLogged()) {
            $this->sessionConfig($this->lifetime);
        }
    }

    protected function setConfig(array $config)
    {
        $config         = array_replace_recursive($this->defaultConfig(), $config);
        $this->lifetime = $config['lifetime'];
    }

    protected function defaultConfig()
    {
        return [
            'lifetime' => 0
        ];
    }

    /**
     * Start session and init data
     */
    private function sessionStart()
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
            $this->exchangeArray(
                array_replace(
                    isset($_SESSION['session']) ? $_SESSION['session'] : [],
                    $this->getArrayCopy()
                )
            );
        }
    }

    /**
     * Return current user registered into session
     * @return User
     */
    public function userLogged()
    {
        $this->sessionStart();

        return $this->isLogged() ? $this['user'] : null;
    }

    /**
     * Check is currently there is some user registered in session
     * @return bool
     */
    public function isLogged()
    {
        return isset($this['user']) && !empty($this['user']);
    }

    /**
     * @param int $lifetime
     */
    private function sessionConfig($lifetime)
    {
        $sessInfo = session_get_cookie_params();
        setcookie(
            session_name(),
            session_id(),
            empty($lifetime) ? 0 : time() + $lifetime,
            $sessInfo["path"],
            $sessInfo["domain"],
            $sessInfo["secure"],
            $sessInfo["httponly"]
        );
    }

    /**
     * Identify a user and register into session if is valid
     * @param string $name
     * @param string $password
     * @return User
     */
    public function login($name, $password)
    {
        $this->logout();

        $user = $this->userRepository->findUserByNameAndPassword($name, $password);

        if (empty($user)) {
            throw new UserNotFoundException();
        }

        $this->sessionConfig($this->lifetime);
        $this->saveUser($user);

        return $user;
    }

    /**
     * Remove current user (if any) from session and
     * close session
     */
    public function logout()
    {
        $this->sessionStart();
        $this->sessionConfig(0);

        $this->saveUser();
    }

    /**
     * Save user into session storage
     * @param User $user
     */
    protected function saveUser(User $user = null)
    {
        $this['user'] = $user;
        $this->close();
    }

    /**
     * Save actual data into session and close
     */
    public function close()
    {
        $this->sessionStart();

        $_SESSION['session'] = $this->getArrayCopy();

        session_write_close();
        unset($_SESSION['session']);
    }
}
