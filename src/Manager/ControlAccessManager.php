<?php

namespace SB\MA\Trial\Manager;

use SB\MA\Trial\Exceptions\AccessDeniedException;
use SB\MA\Trial\Model\Permission;
use SB\MA\Trial\Model\Resource;
use SB\MA\Trial\Model\User;
use SB\MA\Trial\Repository\IUserRepository;

class ControlAccessManager
{
    /**
     * @var IUserRepository
     */
    private $userRepository;
    /**
     * @var AuthorizationManager
     */
    private $authorizationManager;

    public function __construct(IUserRepository $userRepository, AuthorizationManager $authorizationManager)
    {
        $this->userRepository       = $userRepository;
        $this->authorizationManager = $authorizationManager;
    }

    /**
     * @param string $name
     * @param string $password
     * @return User
     * @throws AccessDeniedException
     */
    public function authenticateMe($name, $password)
    {
        $user = $this->userRepository->findUserByNameAndPassword($name, $password);

        if (empty($user)) {
            throw new AccessDeniedException("Access for '" . $name . "' not allowed");
        }

        return $user;
    }

    /**
     * @param Resource $resource
     * @param User $user
     * @param string $action
     * @return Permission[]
     * @throws AccessDeniedException
     */
    public function authorizeMe(Resource $resource, User $user, $action = "GET")
    {
        $permissions = $this->authorizationManager->permissions($resource, $user, $action);

        if (empty($permissions)) {
            throw new AccessDeniedException("Access denied for '" . $user->name() .
                "' to " . $action . "  '" . $resource->name() . "'");
        }

        return $permissions;
    }
}
