<?php

namespace SB\MA\Trial\Manager;

use SB\MA\Trial\Model\Permission;
use SB\MA\Trial\Model\Resource;
use SB\MA\Trial\Model\Role;
use SB\MA\Trial\Model\User;
use SB\MA\Trial\Repository\IAuthorizationRepository;

class AuthorizationManager
{
    /**
     * @var IAuthorizationRepository
     */
    private $authorizationRepository;

    public function __construct(IAuthorizationRepository $authorizationRepository)
    {
        $this->authorizationRepository = $authorizationRepository;
    }

    /**
     * @param Resource $resource
     * @param User $user
     * @param string $action
     * @return Permission[]
     */
    public function permissions(Resource $resource, User $user, $action = "GET")
    {
        $authorization = $this->authorizationRepository
            ->findAuthorizationByResourceName($resource->name());

        // Without authorization can access everybody
        if (empty($authorization)) {
            return [Permission::universal()];
        }

        $permissions = $this->permissionsWithAction(
            $this->permissionsWithRoles(
                $authorization->permissions(),
                $user->roles()
            ),
            $action
        );

        return $permissions;
    }

    /**
     * @param Permission[] $permissions
     * @param string $action
     * @return Permission[]
     */
    protected function permissionsWithAction($permissions, $action)
    {
        return array_filter(
            $permissions,
            function (Permission $permission) use ($action) {
                return in_array($action, $permission->actions()) || in_array('*', $permission->actions());
            }
        );
    }

    /**
     * @param Permission[] $permissions
     * @param Role[] $roles
     * @return Permission[]
     */
    protected function permissionsWithRoles($permissions, $roles)
    {
        return array_filter(
            $permissions,
            function (Permission $permission) use ($roles) {
                return in_array($permission->role(), $roles) || $permission->role() == '*';
            }
        );
    }
}
