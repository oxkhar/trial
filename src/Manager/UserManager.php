<?php

namespace SB\MA\Trial\Manager;

use SB\MA\Trial\Exceptions\UserNotFoundException;
use SB\MA\Trial\Model\User;
use SB\MA\Trial\Model\UUID;
use SB\MA\Trial\Repository\IUserRepository;

class UserManager
{
    /**
     * @var IUserRepository
     */
    private $repository;

    public function __construct(IUserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function listUsers()
    {
        $result = [];
        foreach ($this->repository->findAllUsers() as $user) {
            $result[] = $user;
        }

        return $result;
    }

    /**
     * @param array $data
     * @return User
     */
    public function addNewUser(array $data)
    {
        $data['uid'] = UUID::generate();

        $user = User::bind($data);

        $this->repository->create($user);

        return $user;
    }

    public function updateContent($uid, array $data)
    {
        $data['uid'] = new UUID($uid);

        $user = User::bind($data);

        $this->repository->update($user);

        return $user;
    }

    /**
     *
     * @param string $uid
     * @return User
     * @throws UserNotFoundException
     */
    public function giveMeAUser($uid)
    {
        $user = $this->repository->findUserByUid(new UUID($uid));

        if (empty($user)) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    public function remove($uid)
    {
        $this->repository->delete(new UUID($uid));
    }
}
