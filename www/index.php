<?php

// Fetch method and URI from somewhere
$uri = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '/';
// Avoid "/index.php/something" requests
($_SERVER['SCRIPT_NAME'] === '/' . __FILE__) &&
header('Location: ' . $uri, true, 301) &&
exit;

$loader    = require __DIR__ . '/../vendor/autoload.php';
$container = require __DIR__ . '/../app/dependencies.php';

$httpMethod = $_SERVER['REQUEST_METHOD'];
$dispatcher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) use ($httpMethod) {
    $r->addRoute(
        'GET',
        SB\MA\Trial\Controller\UserController::URI_BASE,
        [SB\MA\Trial\Controller\UserController::class, 'list']
    );
    $r->addRoute(
        'POST',
        SB\MA\Trial\Controller\UserController::URI_BASE,
        [SB\MA\Trial\Controller\UserController::class, $httpMethod]
    );
    $r->addRoute(
        ['GET', 'PUT', 'DELETE'],
        SB\MA\Trial\Controller\UserController::URI_BASE . '/{uid}',
        [SB\MA\Trial\Controller\UserController::class, $httpMethod]
    );
    $r->addRoute(
        ['GET', 'POST'],
        '/login',
        [SB\MA\Trial\Controller\LoginController::class, 'login']
    );
    $r->addRoute(
        'GET',
        '/logout',
        [SB\MA\Trial\Controller\LoginController::class, 'logout']
    );
    // page1, page2, page2
    $r->addRoute(
        'GET',
        '/{page:page(?:1|2|3)}',
        [SB\MA\Trial\Controller\PageController::class, 'index']
    );
});

$routeInfo = $dispatcher->dispatch($httpMethod, rawurldecode($uri));
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        http_response_code(404);
        echo "Not found";
        break;

    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        http_response_code(405);
        header("Allow: " . implode(",", $allowedMethods));
        echo "Method '$httpMethod' not allowed.";
        break;

    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars    = $routeInfo[2];

        $controller = new $handler[0]($container);

        try {
            echo call_user_func_array([$controller, 'action' . $handler[1]], $vars);
        } catch (\SB\MA\Trial\Exceptions\ApplicationException $exc) {
            echo $controller->error($exc->getMessage(), $exc->getCode());
        } catch (\Exception $exc) {
            echo $controller->error($exc->getMessage(), 500);
        }

        break;
}
