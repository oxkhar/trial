<?php

$command = sprintf(
    PHP_BINARY . ' -S %s -t %s %s > /dev/null 2>&1 & echo $!',
    TEST_WEB_SERVER_HOST,
    TEST_WEB_SERVER_DOCROOT,
    TEST_WEB_SERVER_INDEX
);

// Execute the command and store the process ID
$output = [];
exec($command, $output);
$pid = (int)$output[0];

echo sprintf(
        '%s - Web server started on %s with PID %d', date('r'), TEST_WEB_SERVER_HOST, $pid
    ) . PHP_EOL;

// Kill the web server when the process ends
register_shutdown_function(function () use ($pid) {
    echo sprintf('%s - Killing process with ID %d', date('r'), $pid) . PHP_EOL;
    exec('kill ' . $pid);
});

require __DIR__ . "/../vendor/autoload.php";
