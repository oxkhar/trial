<?php

namespace Tests\SB\MA\Trial;

class URLClient
{
    const COOKIE_FILE = __DIR__ . '/../../build/tests/cookies';

    private $host;
    private $header = [];

    public function __construct($host = "localhost:8008")
    {
        $this->host = "http://$host";
    }

    public static function eraseCookies()
    {
        is_file(static::COOKIE_FILE) && unlink(static::COOKIE_FILE);
    }

    /**
     * @see http://php.net/manual/en/function.curl-getinfo.php
     *
     * @param string $location
     * @param array $options
     * @return array
     */
    public function request($location, array $options = null)
    {
        $options = (array)$options;

        $s = curl_init();

        $response = [
            $this->exec($s, $this->host . $location, $options),
            curl_getinfo($s),
            $this->header
        ];

        curl_close($s);

        return $response;
    }

    public function document($location, array $options = null)
    {
        list($content, $info) = $this->request($location, $options);

        return [$this->getDOMDocument($content), $info];
    }

    public function go($location, array $options = null)
    {
        return $this->request($location, $options)[1];
    }

    private function exec($s, $url, $options)
    {
        $this->header = [];

        curl_setopt($s, CURLOPT_URL, $url);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($s, CURLOPT_FOLLOWLOCATION, (isset($options['FOLLOW']) ? !empty($options['FOLLOW']) : true));
        curl_setopt($s, CURLOPT_HEADER, false);
        curl_setopt($s, CURLOPT_HEADERFUNCTION, [$this, 'readHeader']);


        if (!empty($options['COMMAND'])) {
            curl_setopt($s, CURLOPT_CUSTOMREQUEST, $options['COMMAND']);
        }
        if (!empty($options['POST'])) {
            curl_setopt($s, CURLOPT_POST, true);
            curl_setopt($s, CURLOPT_POSTFIELDS, $options['POST']);
        }

        curl_setopt($s, CURLOPT_COOKIEJAR, static::COOKIE_FILE);
        curl_setopt($s, CURLOPT_COOKIEFILE, static::COOKIE_FILE);
        if (!empty($options['AUTHENTICATION'])) {
            curl_setopt($s, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt(
                $s,
                CURLOPT_USERPWD,
                $options['AUTHENTICATION']['user'] . ':' . $options['AUTHENTICATION']['password']
            );
        }

        $response = curl_exec($s);
        if ($response === false) {
            throw new \RuntimeException(curl_error($s), curl_errno($s));
        }

        return $response;
    }

    public function readHeader($s, $line)
    {
        list($header, $value) = array_replace(["", ""], explode(":", $line, 2));
        if (!empty(trim($header))) {
            $this->header[trim($header)] = trim($value);
        }

        return strlen($line);
    }

    private function getDOMDocument($content)
    {
        $html = new \DOMDocument();
        $html->loadHTML($content ?: "<html />");

        return $html;
    }
}
