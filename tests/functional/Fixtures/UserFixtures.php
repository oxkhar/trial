<?php

namespace Tests\SB\MA\Trial\Fixtures;

use SB\MA\Trial\Model\User;
use SB\MA\Trial\Model\UUID;

class UserFixtures
{
    const SCHEMA_FILE = __DIR__ . "/../../../app/db/users-structure.sql";

    /**
     * @var \PDO
     */
    public $dbm;
    private $data;

    public function __construct()
    {
        $config = require __DIR__ . '/../../../app/config_test.php';

        $this->options = $config['database']['users'];

        list($driver, $pathDatabase) = explode(":", $this->options['dsn'], 2);
        is_dir(dirname($pathDatabase)) || mkdir(dirname($pathDatabase), 0777, true);

        $this->dbm = new \PDO($this->options['dsn']);
        $this->dbm->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $this->data();
    }

    protected function data()
    {
        $this->data = [
            [UUID::generate(), 'supercoco', [], '123456'],
            [UUID::generate(), 'caponata', ['PAGE_1'], 'asdfg'],
            [UUID::generate(), 'gustavo', ['PAGE_2'], 'zxcvb'],
            [UUID::generate(), 'epi', ['PAGE_1', 'PAGE_3'], '98765'],
            [UUID::generate(), 'triki', ['ADMIN'], 'qwerty'],
            [UUID::generate(), 'blas', ['PAGE_1', 'PAGE_2', 'PAGE_3'], 'sesammo'],
        ];
    }

    /**
     * @param int $ind
     * @return \ArrayObject
     */
    public function user($ind)
    {
        return $this->newUser($this->data[$ind]);
    }

    /**
     * @param int $ind
     * @return User
     */
    public function userModel($ind)
    {
        return User::bind($this->user($ind));
    }

    /**
     * @return User[]
     */
    public function generateAllUsers()
    {
        $listUsers = [];
        foreach ($this->data as $user) {
            $listUser[] = User::bind($this->newUser($user));
        }

        return $listUser;
    }

    private function newUser($data)
    {
        $user = new \ArrayObject([], \ArrayObject::ARRAY_AS_PROPS);
        list($user->uid, $user->name, $user->roles, $user->password) = $data;

        return $user;
    }

    public function load()
    {
        $this->clean();
        $this->create();
        $this->insert();
    }

    public function create()
    {
        $this->dbm->exec(file_get_contents(static::SCHEMA_FILE));
    }

    public function insert()
    {
        $insert = $this->dbm->prepare(
            'INSERT INTO users (uid, name, roles, password) ' .
            'VALUES (?, ?, ?, ?)'
        );

        foreach ($this->data as $user) {
            $user[2] = json_encode($user[2]);
            $insert->execute($user);
        }
    }

    public function clean()
    {
        $this->dbm->query('DROP TABLE IF EXISTS users');
    }

    public function destroy()
    {
        list($driver, $pathDatabase) = explode(":", $this->options['dsn'], 2);
        is_file($pathDatabase) && unlink($pathDatabase);
        $this->dbm = null;
    }
}
