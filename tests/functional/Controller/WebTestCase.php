<?php

namespace Tests\SB\MA\Trial\Controller;

defined('TEST_WEB_SERVER_HOST') || define("TEST_WEB_SERVER_HOST", getenv('TEST_WEB_SERVER_HOST') ?: "localhost:8008");

use Tests\SB\MA\Trial\Fixtures\UserFixtures;
use Tests\SB\MA\Trial\URLClient;

class WebTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * @var UserFixtures
     */
    protected static $fixtures;
    /**
     * @var URLClient
     */
    protected $urlClient;

    public static function setUpBeforeClass()
    {
        URLClient::eraseCookies();
        static::$fixtures = new UserFixtures();
    }

    protected function setUp()
    {
        $this->urlClient = new URLClient(TEST_WEB_SERVER_HOST);

        static::$fixtures->load();
    }

    protected function tearDown()
    {
        static::$fixtures->clean();
        URLClient::eraseCookies();
    }

    public static function tearDownAfterClass()
    {
        static::$fixtures->destroy();
    }

    protected function loginUser($name, $password)
    {
        $options = [
            'POST' => [
                'name'     => $name,
                'password' => $password
            ]
        ];

        return $this->urlClient->document("/login", $options);
    }

    /**
     *
     * @param int $ing
     * @return \ArrayObject
     */
    protected function fixtureUser($ind)
    {
        return static::$fixtures->user($ind);
    }

    /**
     * @param \DOMDocument $html
     */
    protected function assertIsLoginPage(\DOMDocument $html)
    {
        $this->assertNotEmpty($html->getElementById('login'));
        $this->assertEquals("form", $html->getElementById('login')->tagName);
        $this->assertNotEmpty($html->getElementById('name'));
        $this->assertEquals("input", $html->getElementById('name')->tagName);
        $this->assertEquals("name", $html->getElementById('name')->getAttribute('name'));
        $this->assertNotEmpty($html->getElementById('password'));
        $this->assertEquals("input", $html->getElementById('password')->tagName);
        $this->assertEquals("password", $html->getElementById('password')->getAttribute('name'));
    }

    /**
     * @param \DOMDocument $html
     */
    protected function assertIsErrorPage($message, \DOMDocument $html)
    {
        $this->assertNotEmpty($html->getElementById('message'));
        $this->assertContains($message, $html->getElementById('message')->textContent, '', true);
    }

    /**
     * @param \DOMDocument $html
     */
    protected function assertIsContentPage($page, $user, \DOMDocument $html)
    {
        $this->assertNotEmpty($html->getElementById('page'));
        $this->assertContains($page, $html->getElementById('page')->textContent, '', true);
        $this->assertNotEmpty($html->getElementById('user'));
        $this->assertContains($user, $html->getElementById('user')->textContent, '', true);
    }

    protected function assertIsRedirectedTo($location, $info)
    {
        $this->assertStringEndsWith($location, $info['url']);
        $this->assertGreaterThan(0, $info['redirect_count']);
    }

    protected function assertResponseIsJson($response, $info)
    {
        $this->assertEquals('application/json', $info['content_type']);
        $this->assertJson($response);
    }
}
