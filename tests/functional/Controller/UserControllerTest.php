<?php

namespace Tests\SB\MA\Trial\Controller;

class UserControllerTest extends WebTestCase
{
    /**
     * @test
     */
    public function givenNoAuthenticationHeaderWhenRequestAActionThenAuthenticateNeccesaryResponses()
    {
        list($response, $info, $headers) = $this->urlClient->request('/api/users');

        $this->assertEquals(401, $info['http_code']);
        $this->assertResponseIsJson($response, $info);
        $this->assertArrayHasKey('WWW-Authenticate', $headers);
    }

    /**
     * @test
     */
    public function givenAuthenticationHeaderWhenRequestAActionThenUserIsAuthenticated()
    {
        $options = $this->optionsWithAuthentication();

        list($response, $info, $headers) = $this->urlClient->request('/api/users', $options);

        $this->assertEquals(200, $info['http_code']);
        $this->assertResponseIsJson($response, $info);
    }

    /**
     * @test
     */
    public function whenGetUsersThenListAllUsers()
    {
        $options = $this->optionsWithAuthentication();

        $listUsers = static::$fixtures->generateAllUsers();

        list($response, $info) = $this->urlClient->request('/api/users', $options);

        $this->assertEquals(200, $info['http_code']);
        $this->assertResponseIsJson($response, $info);

        $this->assertCount(count($listUsers), $listUsers);
        $responseUsers = json_decode($response);
        /* @var $user \SB\MA\Trial\Model\User */
        foreach ($listUsers as $user) {
            $data = (object)$user->jsonSerialize();
            $this->assertContains($data, $responseUsers, "", false, false);
        }
    }

    /**
     * @test
     */
    public function givenAUidWhenGetUsersWithThatUidThenReturnUserAssociatedWithThatUid()
    {
        $options = $this->optionsWithAuthentication();

        foreach (static::$fixtures->generateAllUsers() as $user) {
            $expectedJson = json_encode($user);

            list($response, $info) = $this->urlClient->request('/api/users/' . $user->uid(), $options);

            $this->assertEquals(200, $info['http_code']);
            $this->assertResponseIsJson($response, $info);
            $this->assertJsonStringEqualsJsonString($expectedJson, $response);
        }
    }

    /**
     * @test
     */
    public function givenANonExistUidWhenGetUsersThenNotFoundResponse()
    {
        $options = $this->optionsWithAuthentication();

        $uid = "not-exists-id";

        list($response, $info) = $this->urlClient->request('/api/users/' . $uid, $options);

        $this->assertEquals(404, $info['http_code']);
        $this->assertResponseIsJson($response, $info);
    }

    /**
     * @test
     */
    public function givenUserDataWhenPostUsersThenNewUserIsCreated()
    {
        $userData = [
            'name'     => 'name new user',
            'password' => 'password',
            'roles'    => ['PAGE_1', 'PAGE_3']
        ];

        $options = $this->optionsWithAuthentication([
            'COMMAND' => 'POST',
            'POST'    => json_encode($userData)
        ]);

        list($response, $info, $headers) = $this->urlClient->request('/api/users', $options);
        $userNew = json_decode($response, true);

        $this->assertEquals(201, $info['http_code']);
        $this->assertResponseIsJson($response, $info);

        $this->assertArrayHasKey('Location', $headers);
        $this->assertArrayHasKey('Content-Location', $headers);

        $this->assertNotEmpty($userNew['uid']);
        $this->assertArrayNotHasKey('password', $userNew);
        $this->assertEquals($userData['name'], $userNew['name']);
        $this->assertEquals($userData['roles'], $userNew['roles']);
    }

    /**
     * @test
     */
    public function givenWrongFormatDataWhenPostUsersThenReponseBadRequest()
    {
        $options = $this->optionsWithAuthentication([
            'COMMAND' => 'POST',
            'POST'    => "{'name':'Oscar}"
        ]);

        list($response, $info, $headers) = $this->urlClient->request('/api/users', $options);
        $error = json_decode($response, true);

        $this->assertEquals(400, $info['http_code']);
        $this->assertResponseIsJson($response, $info);

        $this->assertArrayHasKey('Date', $headers);

        $this->assertNotEmpty($error['message']);
    }

    /**
     * @test
     */
    public function givenUserIdWhenDeleteThenUserIsRemoved()
    {
        $user = static::$fixtures->userModel(1);

        $options = $this->optionsWithAuthentication([
            'COMMAND' => 'DELETE',
        ]);

        list($response, $info, $headers) = $this->urlClient->request('/api/users/' . $user->uid(), $options);

        $this->assertEquals(204, $info['http_code']);
        $this->assertArrayNotHasKey('Content-Type', $headers);
        $this->assertEmpty($response);
    }

    /**
     * @test
     */
    public function givenAnIdAndDataForAUserWhenPutThenUserUpdatesItsData()
    {
        $user = static::$fixtures->userModel(1);

        $userData = [
            'name'  => 'new name for user',
            'roles' => ['PAGE_1', 'PAGE_2', 'PAGE_3']
        ];

        $options = $this->optionsWithAuthentication([
            'COMMAND' => 'PUT',
            'POST'    => json_encode($userData)
        ]);

        list($response, $info, $headers) = $this->urlClient->request('/api/users/' . $user->uid(), $options);
        $userUpdated = json_decode($response, true);

        $this->assertEquals(204, $info['http_code']);
        $this->assertArrayNotHasKey('Content-Type', $headers);
        $this->assertEmpty($response);
    }

    protected function optionsWithAuthentication($options = [])
    {
        $user = static::$fixtures->userModel(4);

        return array_replace_recursive(
            [
                'AUTHENTICATION' => [
                    'user'     => $user->name(),
                    'password' => $user->password()
                ]
            ],
            $options
        );
    }
}
