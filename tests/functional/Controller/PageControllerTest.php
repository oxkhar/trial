<?php

namespace Tests\SB\MA\Trial\Controller;

use Tests\SB\MA\Trial\Fixtures\UserFixtures;

class PageControllerTest extends WebTestCase
{
    /**
     * @test
     */
    public function givenNoUserLoggedWhenAccessAPageThenIsRedirectedToLoginPage()
    {
        /* @var $html \DOMDocument */
        list($html, $info) = $this->urlClient->document('/page1');

        $this->assertIsRedirectedTo('/login', $info);
        $this->assertIsLoginPage($html);
    }

    /**
     * @test
     * @dataProvider providerUsersWithoutPermissionsOnPages
     */
    public function givenAUserLoggedWithoutPermissionsToThatPageWhenPageIsRequestedThenErrorPageIsShownWithForbbidenStatus(
        $user,
        $page
    ) {
        $this->loginUser($user->name, $user->password);

        /* @var $html \DOMDocument */
        list($html, $info) = $this->urlClient->document($page);

        $this->assertEquals(403, $info['http_code']);
        $this->assertIsErrorPage("Access denied", $html);
    }

    public function providerUsersWithoutPermissionsOnPages()
    {
        $fixtures = new UserFixtures();

        return [
            [$fixtures->user(0), '/page1'],
            [$fixtures->user(1), '/page2'],
            [$fixtures->user(2), '/page3'],
            [$fixtures->user(4), '/page2'],
            [$fixtures->user(3), '/page2'],
        ];
    }

    /**
     * @test
     * @dataProvider providerUsersWithPermissionsOnPages
     */
    public function givenAUserLoggedWithPermissionsPageWhenPageIsRequestedThenPageIsShownWithInfo($user, $page)
    {
        $this->loginUser($user->name, $user->password);

        /* @var $html \DOMDocument */
        list($html, $info) = $this->urlClient->document($page);

        $this->assertEquals(200, $info['http_code']);
        $this->assertIsContentPage(trim($page, '/'), $user->name, $html);
    }

    public function providerUsersWithPermissionsOnPages()
    {
        $fixtures = new UserFixtures();

        return [
            [$fixtures->user(1), '/page1'],
            [$fixtures->user(2), '/page2'],
            [$fixtures->user(3), '/page1'],
            [$fixtures->user(3), '/page3'],
        ];
    }

    /**
     * @test
     */
    public function givenAPageWhenItIsDisplayedThenContentHasAButtonToCloseSession()
    {
        $user = $this->fixtureUser(5);
        $page = '/page1';

        $this->loginUser($user->name, $user->password);

        /* @var $html \DOMDocument */
        list($html, $info) = $this->urlClient->document($page);

        $this->assertNotEmpty($html->getElementById('logout'));
        $this->assertEquals("a", $html->getElementById('logout')->tagName);
        $this->assertStringEndsWith("/logout", $html->getElementById('logout')->getAttribute('href'));
    }
}
