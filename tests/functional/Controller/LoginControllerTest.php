<?php

namespace Tests\SB\MA\Trial\Controller;

class LoginControllerTest extends WebTestCase
{
    /**
     * @test
     */
    public function whenLoginIsRequestThenShowFormToAuthenticate()
    {
        /* @var $html \DOMDocument */
        list($html, $info) = $this->urlClient->document('/login');

        $this->assertIsLoginPage($html);
    }

    /**
     * @test
     */
    public function givenANameAndAPasswordInvalidWhenISubmitFormThenAErrorAndLoginFormAreShown()
    {
        $options = [
            'POST' => [
                'name'     => 'user_not_created',
                'password' => 'invalid_password'
            ]
        ];

        /* @var $html \DOMDocument */
        list($html, $info) = $this->urlClient->document("/login", $options);

        $this->assertIsLoginPage($html);

        $this->assertNotEmpty($html->getElementById('message'));
        $this->assertContains('error', $html->getElementById('message')->nodeValue, '', true);
    }

    /**
     * @test
     */
    public function givenANameAndAPasswordWhenISubmitFormThenMessageOkIsShown()
    {
        $user    = $this->fixtureUser(0);
        $options = [
            'POST' => [
                'name'     => $user->name,
                'password' => $user->password
            ]
        ];

        /* @var $html \DOMDocument */
        list($html, $info) = $this->urlClient->document("/login", $options);

        $this->assertIsLoginPage($html);

        $this->assertNotEmpty($html->getElementById('message'));
        $this->assertContains('ok', $html->getElementById('message')->nodeValue, '', true);
    }

    /**
     * @test
     */
    public function givenLoginWithRefererWhenLoginSucessfullyThenRedirectToRefererLocation()
    {
        $this->urlClient->go("/page1");
        $this->urlClient->go("/login");

        $user    = $this->fixtureUser(1);
        $options = [
            'POST' => [
                'name'     => $user->name,
                'password' => $user->password
            ]
        ];

        $info = $this->urlClient->go("/login", $options);

        $this->assertIsRedirectedTo('/page1', $info);
    }

    /**
     * @test
     */
    public function givenAUserLoggedWhenItRequestsLogoutActionThenUserIsLogout()
    {
        $user = $this->fixtureUser(1);

        $this->loginUser($user->name, $user->password);

        /* @var $html \DOMDocument */
        list($html, $info) = $this->urlClient->document("/logout");
        // Try to asccess a page
        $infoPage = $this->urlClient->go("/page1");

        // If there is no session access a page redirect to login
        $this->assertIsRedirectedTo('/login', $infoPage);

        // Logout redirect to login
        $this->assertIsRedirectedTo('/login', $info);
        $this->assertIsLoginPage($html);
    }
}
