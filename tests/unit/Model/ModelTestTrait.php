<?php

namespace Tests\SB\MA\Trial\Model;

trait ModelTestTrait
{
    protected function assertModelContainsData(array $data, $model)
    {
        foreach ($data as $attr => $value) {
            $this->assertEquals($value, $model->$attr());
        }
    }
}
