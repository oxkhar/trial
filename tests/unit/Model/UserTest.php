<?php

namespace Tests\SB\MA\Trial\Model;

use SB\MA\Trial\Model\User;
use SB\MA\Trial\Model\UUID;

class UserTest extends \PHPUnit_Framework_TestCase
{
    use ModelTestTrait;

    /**
     * @test
     */
    public function userModelIsPopulated()
    {
        $data = [
            "uid"      => UUID::generate(),
            "name"     => "User Name",
            "password" => "A password",
            "roles"    => ["role1", "role2"]
        ];

        $user = new User($data["uid"], $data["name"], $data["roles"]);
        $user->changePassword($data["password"]);

        $this->assertModelContainsData($data, $user);
    }
}
