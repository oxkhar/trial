<?php

namespace Tests\SB\MA\Trial\Model;

use SB\MA\Trial\Model\UUID;

class UUIDTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function generateUniqueIdentifierInstance()
    {
        $uuid = UUID::generate();

        $this->assertInstanceOf(UUID::class, $uuid);
    }

    /**
     * @test
     */
    public function IdentifierCanBeRepresentAsStringFormat()
    {
        $this->assertStringMatchesFormat("%x-%x-%x-%x-%x", "" . UUID::generate());
    }

    /**
     * @test
     */
    public function diferentsIdentifierIsGeneratedEachTime()
    {
        $id = UUID::generate();
        $this->assertNotEquals($id, UUID::generate());
        $this->assertNotEquals($id, UUID::generate());
    }
}
