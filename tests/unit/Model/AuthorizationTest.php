<?php

namespace Tests\SB\MA\Trial\Model;

use SB\MA\Trial\Model\Authorization;
use SB\MA\Trial\Model\Permission;
use SB\MA\Trial\Model\Role;

class AuthorizationTest extends \PHPUnit_Framework_TestCase
{
    use ModelTestTrait;

    /**
     * @test
     */
    public function authorizationModelIsPopulated()
    {
        $data = [
            'location'    => "name",
            'permissions' => [new Permission(new Role("ROLE_1"), ["POST", "PUT"])]
        ];

        $permission = new Authorization($data['location'], $data['permissions']);

        $this->assertModelContainsData($data, $permission);
    }
}
