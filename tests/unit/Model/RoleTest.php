<?php

namespace Tests\SB\MA\Trial\Model;

use SB\MA\Trial\Model\Role;

class RoleTest extends \PHPUnit_Framework_TestCase
{
    use ModelTestTrait;

    /**
     * @test
     */
    public function roleModelIsPopulated()
    {
        $data = [
            "name" => "ROLE_1"
        ];

        $role = new Role("ROLE_1");

        $this->assertModelContainsData($data, $role);
    }
}
