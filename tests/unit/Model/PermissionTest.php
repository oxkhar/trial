<?php

namespace Tests\SB\MA\Trial\Model;

use SB\MA\Trial\Model\Permission;
use SB\MA\Trial\Model\Role;

class PermissionTest extends \PHPUnit_Framework_TestCase
{
    use ModelTestTrait;

    /**
     * @test
     */
    public function permissionModelIsPopulated()
    {
        $data = [
            "role"    => new Role("ROLE_1"),
            "actions" => ["POST"]
        ];

        $permission = new Permission(new Role("ROLE_1"), $data["actions"]);

        $this->assertModelContainsData($data, $permission);
    }
}
