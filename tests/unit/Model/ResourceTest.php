<?php

namespace Tests\SB\MA\Trial\Model;

use SB\MA\Trial\Model\Resource;

class ResourceTest extends \PHPUnit_Framework_TestCase
{
    use ModelTestTrait;

    /**
     * @test
     */
    public function reosurceModelIsPopulated()
    {
        $data = [
            "name" => "title/page",
        ];

        $resource = new Resource($data["name"]);

        $this->assertModelContainsData($data, $resource);
    }
}
