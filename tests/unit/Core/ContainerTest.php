<?php

namespace Tests\SB\MA\Trial\Core;

use SB\MA\Trial\Core\Container;

class ContainerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function servicesAreAssignedLikeAArray()
    {
        $container = new Container();

        $fnc = function () {
            return null;
        };

        $container['service'] = $fnc;

        $this->assertAttributeCount(1, 'container', $container);
        $this->assertAttributeContainsOnly(\Closure::class, 'container', $container);
    }

    /**
     * @test
     */
    public function servicesAreDefinedInACallableElement()
    {
        $service = (object)['service' => 'dummy'];

        $container = new Container();

        $container['service'] = function () use ($service) {
            return $service;
        };

        $this->assertAttributeContainsOnly(\Closure::class, 'container', $container);
    }

    /**
     * @expectedException \RuntimeException
     * @test
     */
    public function whenServiceDefinedHasNotACallableFactoryThrowsException()
    {
        $service = (object)['service' => 'dummy'];

        $container = new Container();

        $container['service'] = $service;

        $this->assertAttributeContainsOnly(\Closure::class, 'container', $container);
    }

    /**
     * @test
     */
    public function servicesAreReturnedWhenIsAccesedByContainer()
    {
        $service = (object)['service' => 'dummy'];

        $container = new Container();

        $container['service'] = function () use ($service) {
            return $service;
        };

        $this->assertInstanceOf(\stdClass::class, $container['service']);
        $this->assertEquals($service, $container['service']);
    }

    /**
     * @test
     */
    public function serviceFactoryCanAccessOtherServicesInItsContext()
    {
        $dependency = (object)['dependency' => 'foo'];

        $container = new Container();

        $container['dependency'] = function () use ($dependency) {
            return $dependency;
        };

        $container['service'] = function () {
            $service = (object)[
                'service'    => 'dummy',
                'dependency' => $this['dependency'],
            ];

            return $service;
        };

        $service = $container['service'];

        $this->assertEquals($dependency, $service->dependency);
    }
}
