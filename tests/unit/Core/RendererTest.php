<?php

namespace Tests\SB\MA\Trial\Core;

use SB\MA\Trial\Core\Renderer;

class RendererTest extends \PHPUnit_Framework_TestCase
{
    const PATH_TEMPLATES = __DIR__ . '/../resources/templates';

    /**
     * @test
     */
    public function renderTextContentFileTemplate()
    {
        $expected = file_get_contents(static::PATH_TEMPLATES . '/hello.php');

        $render = new Renderer(['path' => static::PATH_TEMPLATES]);

        $content = $render->render('hello');

        $this->assertEquals($expected, $content);
    }

    /**
     * @test
     */
    public function renderParamsInTemplateWithItsValues()
    {
        $params = [
            'name'     => 'Supercoco',
            'location' => 'Sesame Street'
        ];

        $render = new Renderer(['path' => static::PATH_TEMPLATES]);

        $content = $render->render('hello-people', $params);

        $this->assertStringContains($params['name'], $content);
        $this->assertStringContains($params['location'], $content);
        $this->assertStringContains('Hello World!', $content);
    }

    protected function assertStringContains($str, $content)
    {
        $this->assertTrue(strpos($content, $str) !== false);
    }
}
