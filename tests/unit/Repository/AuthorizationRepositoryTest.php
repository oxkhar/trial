<?php

namespace Tests\SB\MA\Trial\Repository;

use SB\MA\Trial\Model\Authorization;
use SB\MA\Trial\Model\Permission;
use SB\MA\Trial\Model\Role;
use SB\MA\Trial\Repository\AuthorizationRepository;

class AuthorizationRepositoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AuthorizationRepository
     */
    protected $object;

    /**
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $fixtures = [
            'page1' => [
                'PAGE_1',
                ['ADMIN', 'GET']
            ],
            'page2' => 'PAGE_2',
            'users' => [
                ['ADMIN', "*"],
                ["*", ["GET", "POST"]]
            ]
        ];

        $this->object = new AuthorizationRepository($fixtures);
    }

    /**
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * @dataProvider providerExpectedAuthorizations
     */
    public function testFindAuthorizationByResourceName($resource, $expected)
    {
        $authorization = $this->object->findAuthorizationByResourceName($resource);

        $this->assertNotEmpty($authorization);
        $this->assertInstanceOf(Authorization::class, $authorization);
        $this->assertEquals($expected, $authorization);
    }

    public function providerExpectedAuthorizations()
    {
        return
            [
                [
                    'page1',
                    new Authorization(
                        'page1',
                        [
                            new Permission(new Role('PAGE_1'), ['*']),
                            new Permission(new Role('ADMIN'), ['GET'])
                        ]
                    )
                ],
                [
                    'page2',
                    new Authorization(
                        'page2',
                        [
                            new Permission(new Role('PAGE_2'), ["*"]),
                        ]
                    )
                ],
                [
                    'users',
                    new Authorization(
                        'users',
                        [
                            new Permission(new Role('ADMIN'), ["*"]),
                            new Permission(new Role('*'), ["GET", "POST"])
                        ]
                    )
                ],
            ];
    }
}
