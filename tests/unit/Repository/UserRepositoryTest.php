<?php

namespace Tests\SB\MA\Trial\Repository;

use PDO;
use SB\MA\Trial\Model\User;
use SB\MA\Trial\Model\UUID;
use SB\MA\Trial\Repository\UserRepository;

class UserRepositoryTest extends \PHPUnit_Framework_TestCase
{
    const SCHEMA_FILE   = __DIR__ . "/../../../app/db/users-structure.sql";

    const PATH_DATABASE = ":memory:";

    /**
     * @var \PDO
     */
    protected static $dbm;
    protected static $dataFixtures;
    /**
     * @var UserRepository
     */
    protected $repository;

    public static function setUpBeforeClass()
    {
        static::$dataFixtures = [
            [UUID::generate(), 'supercoco', [], '123456'],
            [UUID::generate(), 'caponata', ['PAGE_1'], 'asdf'],
            [UUID::generate(), 'triki', ['ADMIN'], 'qwerty'],
        ];

        static::$dbm = new PDO('sqlite:' . static::PATH_DATABASE);
        static::$dbm->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->loadFixtures();

        $this->repository = new UserRepository(static::$dbm);
    }

    protected function loadFixtures()
    {
        static::$dbm->query('DROP TABLE IF EXISTS users');

        $ddl = file_get_contents(static::SCHEMA_FILE);
        static::$dbm->exec($ddl);

        $insert = static::$dbm->prepare(
            'INSERT INTO users (uid, name, roles, password) ' .
            'VALUES (?, ?, ?, ?)'
        );

        foreach (static::$dataFixtures as $user) {
            $user[2] = json_encode($user[2]);
            $insert->execute($user);
        }
    }

    /**
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        static::$dbm->query('DROP TABLE IF EXISTS users');
    }

    public static function tearDownAfterClass()
    {
        static::$dbm = null;
    }

    /**
     * @test
     */
    public function findUserByNameAndPassword()
    {
        list($uid, $name, $roles, $password) = static::$dataFixtures[1];

        $user = $this->repository->findUserByNameAndPassword($name, $password);

        $this->assertNotEmpty($user);
        $this->assertEquals($uid, $user->uid());
        $this->assertEquals($name, $user->name());
        $this->assertEquals($password, $user->password());
        $this->assertEquals($roles, $user->roles());
    }

    /**
     * @test
     */
    public function findUserByUid()
    {
        list($uid, $name, $roles, $password) = static::$dataFixtures[1];

        $user = $this->repository->findUserByUid($uid);

        $this->assertNotEmpty($user);
        $this->assertEquals($uid, $user->uid());
        $this->assertEquals($name, $user->name());
        $this->assertEquals($password, $user->password());
        $this->assertEquals($roles, $user->roles());
    }

    /**
     * @test
     */
    public function findAll()
    {
        $users = iterator_to_array(
            $this->repository->findAllUsers()
        );

        foreach (static::$dataFixtures as $data) {
            $user = new User(...$data);
            $user->changePassword($data[3]);
            $this->assertContains($user, $users, "", false, false);
        }
    }

    /**
     * @test
     */
    public function createNewRecord()
    {
        $uid = UUID::generate();

        $userExpected = new User($uid, 'new user', ['PAGE_1', 'PAGE_2']);
        $userExpected->changePassword('123456');

        $this->repository->create($userExpected);

        $user = $this->repository->findUserByUid($uid);

        $this->assertEquals($userExpected, $user);
    }

    /**
     * @test
     */
    public function updateDataForAnExistentUser()
    {
        $uid = static::$dataFixtures[1][0];

        $userExpected = new User($uid, 'user modified', ['PAGE_3', 'ADMIN']);

        $this->repository->update($userExpected);

        $user = $this->repository->findUserByUid($uid);

        $this->assertEquals($userExpected->name(), $user->name());
        $this->assertEquals($userExpected->roles(), $user->roles());
    }

    /**
     * @test
     */
    public function removeAUser()
    {
        $uid = static::$dataFixtures[1][0];

        $userExpected = new User($uid, 'user modified', '99988889', ['PAGE_3', 'ADMIN']);

        $this->repository->delete($uid);

        $user = $this->repository->findUserByUid($uid);

        $this->assertEmpty($user);
    }
}
