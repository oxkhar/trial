<?php

namespace Tests\SB\MA\Trial\Manager;

use SB\MA\Trial\Manager\AuthorizationManager;
use SB\MA\Trial\Model\Authorization;
use SB\MA\Trial\Model\Permission;
use SB\MA\Trial\Model\Resource;
use SB\MA\Trial\Model\Role;
use SB\MA\Trial\Model\User;
use SB\MA\Trial\Model\UUID;
use SB\MA\Trial\Repository\IAuthorizationRepository;

class AuthorizationManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function resourceWithoutRolesIsAccesibleByAllUsers()
    {
        $user = User::bind([
            'uid'      => UUID::generate(),
            'name'     => 'User 1',
            'password' => 'Password',
            'roles'    => ["PAGE_1"]
        ]);

        $resource = new Resource("resource");

        $authorizationRepository = $this->createMock(IAuthorizationRepository::class);

        $sut = new AuthorizationManager($authorizationRepository);

        $permissions = $sut->permissions($resource, $user);

        $this->assertNotEmpty($permissions);
    }

    /**
     * @test
     */
    public function userWithoutRolesCannotAccessToAnyResourceWithRoles()
    {
        $user = User::bind([
            'uid'      => UUID::generate(),
            'name'     => 'User 1',
            'password' => 'Password',
            'roles'    => []
        ]);

        $resource = new Resource("resource");

        $authorization = new Authorization(
            'resourceName',
            [new Permission(new Role('PAGE_1'))]
        );

        $authorizationRepository = $this->createMock(IAuthorizationRepository::class);
        $authorizationRepository
            ->expects($this->any())
            ->method('findAuthorizationByResourceName')
            ->willReturn($authorization)
        ;

        $sut = new AuthorizationManager($authorizationRepository);

        $permissions = $sut->permissions($resource, $user);

        $this->assertEmpty($permissions);
    }

    /**
     * @test
     */
    public function resourceIsAccessibleWhenAUserHasTheSameRoleThatSomeOfItsPermissions()
    {
        $user = User::bind([
            'uid'      => UUID::generate(),
            'name'     => 'User 1',
            'password' => 'Password',
            'roles'    => ["PAGE_1"]
        ]);

        $resource = new Resource("resource");

        $authorization = new Authorization(
            'resourceName',
            [new Permission(new Role('PAGE_1'))]
        );

        $authorizationRepository = $this->createMock(IAuthorizationRepository::class);
        $authorizationRepository->expects($this->any())
                                ->method('findAuthorizationByResourceName')
                                ->willReturn($authorization)
        ;

        $sut = new AuthorizationManager($authorizationRepository);

        $permissions = $sut->permissions($resource, $user);

        $this->assertNotEmpty($permissions);
    }

    /**
     * @test
     */
    public function actionOverResourceIsPossibleWhenPermissionHasThatAction()
    {
        $user = User::bind([
            'uid'      => UUID::generate(),
            'name'     => 'User 1',
            'password' => 'Password',
            'roles'    => ["PAGE_1"]
        ]);

        $resource = new Resource("resource");

        $authorization = new Authorization(
            'resourceName',
            [new Permission(new Role('PAGE_1'), ["POST", "PUT"])]
        );

        $authRepository = $this->createMock(IAuthorizationRepository::class);
        $authRepository->expects($this->any())
                       ->method('findAuthorizationByResourceName')
                       ->willReturn($authorization)
        ;

        $sut = new AuthorizationManager($authRepository);

        $withPermissions    = $sut->permissions($resource, $user, "PUT");
        $withoutPermissions = $sut->permissions($resource, $user, "DELETE");

        $this->assertNotEmpty($withPermissions);
        $this->assertEmpty($withoutPermissions);
    }

    /**
     * @test
     */
    public function asteriskCanDefineAnyRoleOrAction()
    {
        $user = User::bind([
            'uid'      => UUID::generate(),
            'name'     => 'User 1',
            'password' => 'Password',
            'roles'    => ["PAGE_1"]
        ]);

        $resource = new Resource("resource");

        $authorization = new Authorization(
            'resourceName',
            [new Permission(new Role('*'), ["*"])]
        );

        $authorizationRepository = $this->createMock(IAuthorizationRepository::class);
        $authorizationRepository->expects($this->any())
                                ->method('findAuthorizationByResourceName')
                                ->willReturn($authorization)
        ;

        $sut = new AuthorizationManager($authorizationRepository);

        $permissions = $sut->permissions($resource, $user, "PUT");

        $this->assertNotEmpty($permissions);
    }
}
