<?php

namespace Tests\SB\MA\Trial\Manager;

use SB\MA\Trial\Exceptions\UserNotFoundException;
use SB\MA\Trial\Manager\SessionManager;
use SB\MA\Trial\Model\User;
use SB\MA\Trial\Model\UUID;
use SB\MA\Trial\Repository\IUserRepository;

/**
 * @runTestsInSeparateProcesses
 */
class SessionManagerTest extends \PHPUnit_Framework_TestCase
{
    const SESSION_FILES_PATH = __DIR__ . "/../../../build/tests/sessions";

    public static function setUpBeforeClass()
    {
        !is_dir(self::SESSION_FILES_PATH) &&
        mkdir(self::SESSION_FILES_PATH, 0777, true);
        // Don't send any cookie about session
        ini_set("session.use_cookies", 0);
        ini_set("session.use_only_cookies", 0);
        ini_set("session.use_trans_sid", 1);
        ini_set("session.cache_limiter", null);
        ini_set("session.save_handler", "files");
        ini_set("session.save_path", self::SESSION_FILES_PATH);
    }

    public function setUp()
    {
        static::removeSessions();
    }

    public function tearDown()
    {
        // Hack to end test without error about output buffers
        ob_get_status() && ob_end_flush();
        !ob_get_status() && ob_start();
    }

    public static function tearDownAfterClass()
    {
        static::removeSessions();
    }

    /**
     * @test
     */
    public function sessionConfigByDefaultWheNoSetupConfig()
    {
        $session = new SessionManager($this->createMock(IUserRepository::class));

        $this->assertAttributeSame(0, 'lifetime', $session);
    }

    /**
     * @test
     */
    public function setupConfigSessionSettingNewValues()
    {
        $config  = [
            'lifetime' => 200
        ];
        $session = new SessionManager($this->createMock(IUserRepository::class), $config);

        $this->assertAttributeSame($config['lifetime'], 'lifetime', $session);
    }

    /**
     * @test
     */
    public function sessionIsAccesibleWhenIsInstanciated()
    {
        $session = $this->getMockSession();

        $this->assertTrue(session_status() === PHP_SESSION_ACTIVE);
    }

    /**
     * @test
     */
    public function sessionIsCloseWhenUserChangeStatus()
    {
        $userLogged = $this->saveUserInSession();

        $this->assertTrue(session_status() !== PHP_SESSION_ACTIVE);

        $session = $this->getMockSession();

        $session->logout();

        $this->assertTrue(session_status() !== PHP_SESSION_ACTIVE);
    }

    /**
     * @test
     */
    public function whenLoginAUserCorrectlyThenUserIsSavedIntoSession()
    {
        $userLogged = $this->saveUserInSession();

        $session = $this->getMockSession();

        $this->assertArrayHasKey('user', $session);
        $this->assertNotEmpty($userLogged);
        $this->assertEquals($userLogged, $session->user);
    }

    /**
     * @test
     */
    public function whenLoginIsFailedThenUserIsNotRegisteredInSession()
    {
        $stubRepository = $this->createMock(IUserRepository::class);
        $stubRepository->expects($this->any())
                       ->method('findUserByNameAndPassword')
                       ->willReturn(false)
        ;

        $session = new SessionManager($stubRepository);
        try {
            $userLogged = $session->login('user_not_found', 'failed_password');
        } catch (UserNotFoundException $esc) {
            $userLogged = null;
        }

        $newSession = $this->getMockSession();

        $this->assertEmpty($userLogged);
        $this->assertEquals($userLogged, $newSession->user);
    }

    /**
     * @test
     */
    public function userLoggedIsAccesibleEachTimeSessionIsInitialized()
    {
        $user = $this->saveUserInSession();

        $session = $this->getMockSession();

        $userLogged = $session->userLogged();

        $this->assertNotEmpty($userLogged);
        $this->assertEquals($user, $userLogged);
    }

    /**
     * @test
     */
    public function logoutRemoveUserFromSession()
    {
        $user = $this->saveUserInSession();

        $session = $this->getMockSession();

        $session->logout();

        $newSession = $this->getMockSession();
        $userLogged = $newSession->userLogged();

        $this->assertEmpty($newSession['user']);
        $this->assertEmpty($userLogged);
    }

    /**
     * @test
     */
    public function canCheckIfAUserIsLogged()
    {
        $user = $this->saveUserInSession();

        $session  = $this->getMockSession();
        $isLogged = $session->isLogged();
        $session->logout();

        $newSession = $this->getMockSession();

        $notIsLogged = $newSession->isLogged();

        $this->assertTrue($isLogged);
        $this->assertFalse($notIsLogged);
    }

    /**
     * @test
     */
    public function dataSessionIsStoredWhenSessionIsClosed()
    {
        $data = "data is stored";

        $session = $this->getMockSession();

        $session->data = $data;

        $session->close();
        unset($_SESSION['session']);

        $session->moreData = $data . $data;
        $session->close();

        $newSession = $this->getMockSession();

        $this->assertEquals($data, $newSession->data);
        $this->assertEquals($data . $data, $newSession->moreData);
    }

    protected function saveUserInSession()
    {
        $user = User::bind([
            'uid'      => UUID::generate(),
            'name'     => 'supercoco',
            'password' => '123456',
            'roles'    => []
        ]);

        $stubRepository = $this->createMock(IUserRepository::class);
        $stubRepository->expects($this->any())
                       ->method('findUserByNameAndPassword')
                       ->with($this->equalTo($user->name()), $this->equalTo($user->password()))
                       ->willReturn($user)
        ;

        $session = new SessionManager($stubRepository);

        return $session->login($user->name(), $user->password());
    }

    protected static function removeSessions()
    {
        // Discard any session and file
        (session_status() === PHP_SESSION_ACTIVE) && session_destroy();
        if (!is_dir(self::SESSION_FILES_PATH)) {
            return;
        }
        foreach (new \DirectoryIterator(self::SESSION_FILES_PATH) as $file) {
            !$file->isDot() && unlink($file->getPathname());
        }
    }

    protected function getMockSession()
    {
        return new SessionManager(
            $this->createMock(IUserRepository::class),
            ['lifetime' => 100]
        );
    }
}
