<?php

namespace Tests\SB\MA\Trial\Manager;

use SB\MA\Trial\Manager\UserManager;
use SB\MA\Trial\Model\User;
use SB\MA\Trial\Model\UUID;
use SB\MA\Trial\Repository\IUserRepository;

class UserManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function bindDataFromArrayStructrueToUserEntity()
    {
        $mockRepository = $this->createMock(IUserRepository::class);

        $manager = new UserManager($mockRepository);

        $data = ['uid'      => UUID::generate(),
                 'name'     => 'supercoco',
                 'password' => '123456',
                 'roles'    => ['PAGE_1', 'PAGE_2']
        ];

        $user = User::bind($data);

        $this->assertInstanceOf(User::class, $user);
        $this->assertModelContainsData($data, $user);
    }

    /**
     * @test
     */
    public function givenUserDataWhenAddNewUserThenUserIsCreatedInRepository()
    {
        $data = ['name' => 'supercoco', 'password' => '123456', 'roles' => ['PAGE_1', 'PAGE_2']];

        $mockRepository = $this->createMock(IUserRepository::class);
        $manager        = new UserManager($mockRepository);

        $mockRepository->expects($this->once())
                       ->method('create')
                       ->with($this->callback(function ($model) use ($data) {
                           return $this->assertModelContainsData($data, $model);
                       }))
        ;

        $manager->addNewUser($data);
    }

    /**
     * @test
     */
    public function givenUserDataAndIdForAExisitentUserWhenUpdateContentForThatUserThenDataIsUpdatedInTheRepository()
    {
        $uid  = UUID::generate();
        $data = ['name' => 'supercoco', 'password' => '123456', 'roles' => ['PAGE_1', 'PAGE_2']];

        $mockRepository = $this->createMock(IUserRepository::class);
        $manager        = new UserManager($mockRepository);

        $mockRepository->expects($this->once())
                       ->method('update')
                       ->with($this->callback(function ($model) use ($uid, $data) {
                           $data = array_replace($data, ['uid' => $uid]);

                           return $this->assertModelContainsData($data, $model);
                       }))
        ;

        $manager->updateContent($uid, $data);
    }

    /**
     * @test
     */
    public function givenAUserIdentifierWhenIRequestAUserThenRepositoryGiveMeAUser()
    {
        $uid  = UUID::generate();
        $user = User::bind([
            'uid'      => $uid,
            'name'     => 'supercoco',
            'password' => '123456',
            'roles'    => ['PAGE_1', 'PAGE_2']
        ]);

        $mockRepository = $this->createMock(IUserRepository::class);
        $manager        = new UserManager($mockRepository);

        $mockRepository->expects($this->once())
                       ->method('findUserByUid')
                       ->with($this->equalTo($uid))
                       ->willReturn($user)
        ;

        $manager->giveMeAUser($uid);
    }

    /**
     * @test
     */
    public function givenAUserIdentifierWhenIsRemovedThenItWillBeRemovedFromRepository()
    {
        $uid  = UUID::generate();
        $user = User::bind([
            'uid'      => $uid,
            'name'     => 'supercoco',
            'password' => '123456',
            'roles'    => ['PAGE_1', 'PAGE_2']
        ]);

        $mockRepository = $this->createMock(IUserRepository::class);
        $manager        = new UserManager($mockRepository);

        $mockRepository->expects($this->once())
                       ->method('delete')
                       ->with($this->equalTo($uid))
        ;

        $manager->remove($uid);
    }

    private function assertModelContainsData($data, $model)
    {
        foreach ($data as $field => $value) {
            $this->assertEquals($data[$field], $model->$field());
        }

        return true;
    }
}
