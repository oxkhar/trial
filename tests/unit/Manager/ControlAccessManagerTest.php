<?php

namespace Tests\SB\MA\Trial\Manager;

use SB\MA\Trial\Manager\AuthorizationManager;
use SB\MA\Trial\Manager\ControlAccessManager;
use SB\MA\Trial\Model\Authorization;
use SB\MA\Trial\Model\Permission;
use SB\MA\Trial\Model\Resource;
use SB\MA\Trial\Model\Role;
use SB\MA\Trial\Model\User;
use SB\MA\Trial\Model\UUID;
use SB\MA\Trial\Repository\IAuthorizationRepository;
use SB\MA\Trial\Repository\IUserRepository;

class ControlAccessManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function givenAUserAndPasswordValidWhenAuthenticatesThenUserIsReturned()
    {
        $user = User::bind([
            'uid'      => UUID::generate(),
            'name'     => 'User 1',
            'password' => 'Password',
            'roles'    => ["PAGE_1"]
        ]);

        $userRepository       = $this->createMock(IUserRepository::class);
        $authorizationManager = $this->getMockBuilder(AuthorizationManager::class)
                                     ->disableOriginalConstructor()
                                     ->getMock()
        ;

        $sut = new ControlAccessManager($userRepository, $authorizationManager);

        $userRepository->expects($this->once())
                       ->method('findUserByNameAndPassword')
                       ->with($user->name(), $user->password())
                       ->willReturn($user)
        ;

        $user = $sut->authenticateMe($user->name(), $user->password());

        $this->assertNotEmpty($user);
        $this->assertInstanceOf(User::class, $user);
    }

    /**
     * @test
     * @expectedException \SB\MA\Trial\Exceptions\AccessDeniedException
     */
    public function givenAUserAndPasswordNoExistWhenAuthenticatesThenThrowAccessDeniedException()
    {
        $user = User::bind([
            'uid'      => UUID::generate(),
            'name'     => 'User 1',
            'password' => 'Password',
            'roles'    => ["PAGE_1"]
        ]);

        $userRepository       = $this->createMock(IUserRepository::class);
        $authorizationManager = $this->getMockBuilder(AuthorizationManager::class)
                                     ->disableOriginalConstructor()
                                     ->getMock()
        ;

        $sut = new ControlAccessManager($userRepository, $authorizationManager);

        $user = $sut->authenticateMe($user->name(), $user->password());
    }

    /**
     * @test
     */
    public function givenAUserWithPermissionsAboutAReourcesWhenAuthorizeUserThenPermissionsOverResourceAreReturned()
    {
        $user = User::bind([
            'uid'      => UUID::generate(),
            'name'     => 'User 1',
            'password' => 'Password',
            'roles'    => ["PAGE_1"]
        ]);

        $authorization = new Authorization('resource', [Permission::universal()]);
        $resource      = new Resource('resource');

        $userRepository          = $this->createMock(IUserRepository::class);
        $authorizationRepository = $this->createMock(IAuthorizationRepository::class);
        $authorizationManager    = new AuthorizationManager($authorizationRepository);

        $sut = new ControlAccessManager($userRepository, $authorizationManager);

        $authorizationRepository->expects($this->once())
                                ->method('findAuthorizationByResourceName')
                                ->with('resource')
                                ->willReturn($authorization)
        ;

        $permissions = $sut->authorizeMe($resource, $user, 'GET');

        $this->assertNotEmpty($permissions);
    }

    /**
     * @test
     * @expectedException \SB\MA\Trial\Exceptions\AccessDeniedException
     */
    public function givenAUserWithoutAccessToResourceWhenAuthorizeUserThenThrowAccessDeniedException()
    {
        $user = User::bind([
            'uid'      => UUID::generate(),
            'name'     => 'User 1',
            'password' => 'Password',
            'roles'    => ["PAGE_1"]
        ]);

        $resource      = new Resource('resource');
        $authorization = new Authorization('resource', [new Permission(new Role('OTHER'))]);

        $userRepository          = $this->createMock(IUserRepository::class);
        $authorizationRepository = $this->createMock(IAuthorizationRepository::class);
        $authorizationManager    = new AuthorizationManager($authorizationRepository);

        $sut = new ControlAccessManager($userRepository, $authorizationManager);

        $authorizationRepository->expects($this->once())
                                ->method('findAuthorizationByResourceName')
                                ->with('resource')
                                ->willReturn($authorization)
        ;


        $user = $sut->authorizeMe($resource, $user);
    }
}
