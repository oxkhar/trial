<!DOCTYPE html>
<html>
<head><title><?= $page->name() ?></title></head>
<body>
    <h1>Hello World!</h1>

    <div id="page">I'm in <?= $page->name() ?></div>

    <div id="user">I'm "<?= $user->name() ?>" user</div>

    <div><a href="/logout" id="logout">Logout</a></div>

</body>
</html>
