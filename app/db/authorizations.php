<?php

/**
 * Authorization access config...
 * [
 *    {resource-name} => [
 *          [ {role-name}, {action} ],
 *          ...
 *     ],
 *     ...
 * ]
 *
 * If it's only one value you can set without array sintax,
 * and you can ommit {action} to a global access...
 * [
 *    {resource-name} => [ {role-name}, {action} ],
 *    {resource-name} => {role-name},
 * ]
 *
 * {action}: GET|POST|PUT|DELETE
 */
return [
    'page1' => 'PAGE_1',
    'page2' => 'PAGE_2',
    'page3' => 'PAGE_3',
    'users' => [
        ['ADMIN', "*"],
        ["*", "GET"]
    ]
];
