
DROP TABLE IF EXISTS users;

CREATE TABLE users (
  uid varchar(64) NOT NULL,
  name varchar(128) NOT NULL UNIQUE,
  password varchar(128) NOT NULL,
  roles varchar(128),
  PRIMARY KEY (uid)
);
