<?php

defined('APP_PATH') || define('APP_PATH', __DIR__);
defined('WEB_PATH') || define('WEB_PATH', realpath(__DIR__ . '../www'));

return
[
    'template' => [
        // Path to search temlates
        'path' => APP_PATH . '/views'
    ],
    'database' => [
        'users'          => [
            'dsn'      => 'sqlite:' . APP_PATH . '/../build/data/users.db',
            'user'     => null,
            'password' => null,
            'options'  => []
        ],
        'authorizations' => [
            'data' => APP_PATH . '/db/authorizations.php'
        ]
    ],
    'session'  => [
        // Seconds to end login user, 0 until close nav
        'lifetime' => 5 * 60
    ]
];
