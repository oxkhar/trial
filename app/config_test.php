<?php

return array_replace_recursive(
    include __DIR__ . '/config.php',
    [
        'database' => [
            'users' => [
                'dsn' => 'sqlite:' . __DIR__ . '/../build/tests/tests_users.db'
            ]
        ]
    ]
);
