<?php

$config = require __DIR__ . '/config' . (defined('ENV') ? '_' . ENV : '') . '.php';

$container = new \SB\MA\Trial\Core\Container();

$container['render'] = function () use ($config) {
    return new \SB\MA\Trial\Core\Renderer($config['template']);
};

$container['userDbm'] = function () use ($config) {
    $usersDb = $config['database']['users'];

    $dbm = new PDO(
        $usersDb['dsn'],
        $usersDb['user'],
        $usersDb['password'],
        $usersDb['options']
    );
    $dbm->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $dbm;
};

$container['userRepository'] = function () use ($config) {
    return new \SB\MA\Trial\Repository\UserRepository($this["userDbm"]);
};

$container['session'] = function () use ($config) {
    return new \SB\MA\Trial\Manager\SessionManager($this['userRepository'], $config['session']);
};

$container['authorizationRepository'] = function () use ($config) {
    $authorizations = include $config['database']['authorizations']['data'];

    return new \SB\MA\Trial\Repository\AuthorizationRepository($authorizations);
};

$container['authorizationManager'] = function () use ($config) {
    return new \SB\MA\Trial\Manager\AuthorizationManager($this["authorizationRepository"]);
};

$container['controlAccess'] = function () use ($config) {
    return new \SB\MA\Trial\Manager\ControlAccessManager($this["userRepository"], $this["authorizationManager"]);
};

$container['userManager'] = function () use ($config) {
    return new SB\MA\Trial\Manager\UserManager($this["userRepository"]);
};

return $container;
